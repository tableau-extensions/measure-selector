import React from 'react';
import ReactDOM from 'react-dom';
import { Route, Link, BrowserRouter as Router } from 'react-router-dom'
import './index.css';
import App from './main/App';
import Config from './popup/config';

const routerOptions = process.env.NODE_ENV === "production" ? { basename: process.env.PUBLIC_URL } : {};

const routing = (
  <Router {...routerOptions}>
    <div>
      <Route exact path="/" component={App} />
      <Route path="/config" component={Config} />
    </div>
  </Router>
)


//	Render to DOM
ReactDOM.render(routing, document.getElementById('root'))