import React, { Component } from 'react';
import { Tabs, TextField, Spinner } from '@tableau/tableau-ui';
import './App.css';

 /* global tableau */

 // temporary constants, will need to change to use the config menu
 const tempSettings = {
  'measuresSheet' : 'Measures',
  'parameter': 'SelectedMeasure'
 }

class App extends Component {

  /*  Init the Component  */
  constructor(props){
    super(props);

    //  Init the component's state
    this.state = { 
      selectedTabIndex: 0,
      settings: tempSettings,
      initCounter: 0,
      error: null,
      parameter: null,
      data: []
    };

    //  Add bindings for class functions
    this.log = this.log.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.querySheet = this.querySheet.bind(this);
    this.configure = this.configure.bind(this);
  }

  //  Logger
  log(message){
    
    console.log('Tableau Extension: ' + message + ' ( ' + new Date());
  }

  //  Define the click handler for this extension
  handleClick(index){

    //  Save a reference to THIS
    let myComponent = this;

    //  Figure out the new selection
    let selection = myComponent.state.data[index].title;

    //  Update the parameter on the dashboard
    myComponent.state.parameter.changeValueAsync(selection).then(() => {
      
      //  Update the state, with the current selection
      myComponent.setState({ selectedTabIndex: index });

    })
  }

  //  Function for checking parameters/data
  querySheet(){

    //  Save a reference to THIS
    let myComponent = this;

    //  Find the measures worksheet
    let sheet = tableau.extensions.dashboardContent.dashboard.worksheets.find( w => w.name === myComponent.state.settings.measuresSheet ); 

    //  Make sure we found a matching sheet for the measures
    if (sheet){

      //  Find all relevant parameters for this worksheet
      myComponent.log('Looking for parameters...');
      sheet.getParametersAsync().then( parameters => {

        myComponent.log('...parameters found');
        //  Find the specific parameter to use
        let parameter = parameters.find( p => p.name === myComponent.state.settings.parameter);

        //  Make sure the parameter exists still
        if (parameter) {

          //  Get the summary data to show as tabs
          myComponent.log('Looking for sheet data...');
          sheet.getSummaryDataAsync().then( response => {
            
            myComponent.log('...Sheet data found');
            //  Make sure the measure sheet has data
            if (response.data && response.data.length>0) {

              //  Init an array to hold the measures
              let measures = [];

              //  Make sure to keep track of which measure is selected
              let selectionIndex = null;

              //  Loop through each data point
              response.data.forEach( (measure,idx) => {

                //  Parse the measure's name and value
                let name = measure[0].formattedValue,
                    value = measure[1].formattedValue;

                //  Create an object to describe th measure
                let myMeasure = {
                  'title' : name,
                  'content' : name + ':' + value,
                  'selected' : name === parameter.currentValue.formattedValue
                }

                myComponent.log('Found measure: ' + myMeasure.title);

                //  Check if selected
                if (name === parameter.currentValue.formattedValue){
                  selectionIndex = idx;
                }

                //  Add measure to the array
                measures.push(myMeasure);

              })

              //  Update the state
              myComponent.setState({
                'data': measures,
                'parameter': parameter,
                'selectedTabIndex': selectionIndex
              })

            } else {

              //  Handle the case, for when the measure sheet has no data
              myComponent.setState({'error':'No data found in ' + myComponent.state.settings.measuresSheet + ' sheet'});
            }
          })

        } else {

          //  No parameter found with this name
          myComponent.setState({'error':'Parameter ' + myComponent.state.settings.parameter + ' not found'});
        }

      })
    } else {
      //  Measures sheet not found, report the error
      myComponent.setState({'error': myComponent.state.settings.measuresSheet + ' sheet not found on this dashboard'});
    }
  }

  //  Function to handle the config popup
  configure() {

    //  Save a reference to THIS
    let myComponent = this;

    //  Define the location of the configuration window
    const popupUrl = window.location.origin + window.location.pathname + 'config.html';

    //  Display the popup dialog
    tableau.extensions.ui.displayDialogAsync(popupUrl, JSON.stringify(myComponent.state.settings), { height: 500, width: 500 })
      .then(() => {

        //  Get the new settings
        let newSettings = tableau.extensions.settings.getAll();
        
        // Popup was closed gracefully, save the state and re-query the sheet
        myComponent.setState(
          {'settings':newSettings},
          myComponent.querySheet
        )
        
      }).catch((error) => {
        
        //  Something went wrong, log to console
        myComponent.log("Error: " + error);
      });
  }

  /*  Initialize the connection to Tableau  */
  componentDidMount() {

    //  Save a reference to THIS
    let myComponent = this;

    //  Initialize the connection to Tableau
    myComponent.log('Initializing...');
    tableau.extensions.initializeAsync({'configure': myComponent.configure}).then(() => {


      //  Add event handler, in case the settings are changed
      tableau.extensions.settings.addEventListener(tableau.TableauEventType.SettingsChanged, (settingsEvent) => {
        myComponent.setState(
            {'settings':settingsEvent.newSettings},
            myComponent.querySheet
        )
      });

      //  Done initializing, now get the data from the sheet
      myComponent.log('...Initializing Complete');
      myComponent.querySheet();
    })
  }

  /*  HTML Renderer */
  render() {

    //  Show tabs or an error message
    let content;
    if (this.state.error) {
      content = <TextField errorMessage={true} label="Error" value={this.state.error}
                           style={{'width':'100%'}} readOnly>
                </TextField>;
    } else {
      content = <Tabs onTabChange={this.handleClick} 
                  selectedTabIndex={this.state.selectedTabIndex} tabs={this.state.data}>
                </Tabs>;
    }
    return (
      <div className="App">
        {content}
      </div>
    );
  }
}

export default App;
