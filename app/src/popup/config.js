import React, { Component } from 'react'
import { Button, Checkbox, DropdownSelect, Pill, Radio, TextField } from '@tableau/tableau-ui';
import './config.css';

 /* global tableau */

//	Define popup config dialog class
class Config extends Component {

	/*	Initialize the component 	*/
	constructor(props){
		super(props)

		//	Initialize the state
		this.state = {
			'worksheet': '',
			'worsheets':[],
			'parameter': '',
			'parameters': [],
			'ready': false,
			'dev': false
		}

		//	Add function bindings to this component
		this.saveSettings = this.saveSettings.bind(this);
		this.initTableau = this.initTableau.bind(this);
	}

	/*	Load from Tableau, when the component first mounts 	*/
	componentDidMount(){

		//	Run code to load the tableau connection
		if (!this.state.dev) {
			this.initTableau();
		}		
	}

	//	Define a function
	initTableau(){

		//	Save a reference to THIS
		let myComponent = this;

		//	Initialize the dialog window from Tableau
		tableau.extensions.initializeDialogAsync()
			.then( currentSettingsString => {

				//	get the current selections
				let currentSettings = JSON.parse(currentSettingsString);

				//	Define a temporary array to hold the list of worksheets
				let sheets = [];

				//	Check to see if any are already selected
				let selectedSheetIdx = null;

				//	Get the list of worksheets on this dashboard
				let worksheets = tableau.extensions.dashboardContent.dashboard.worksheets;

				//	Parse through them, and save only the names
				worksheets.forEach( (sheet,idx) => {

					//	Check for selection
					if (currentSettings.measuresSheet === sheet.name) {
						selectedSheetIdx = idx;
					}

					//	Save the sheet to the list
					sheets.push(sheet.name);
				})

				//	Query for the list of parameters
				tableau.extensions.dashboardContent.dashboard.getParametersAsync()
					.then( parameters => {

						//	Define a temporary array to store parameters
						let ps = []

						//	Check to see if a parameter is already selected
						let selectedParameterIdx = null;

						//	Loop through each parameter
						parameters.forEach( (param, idx) => {

							//	Check for Selection
							if (param.name === currentSettings.parameter) {
								selectedParameterIdx = idx;
							}

							//	Save the parameter to the lit
							ps.push(param.name);
						})
						
						//	All worksheets and parameters fetched, save the state
						myComponent.setState({
							'worksheets': sheets,
							'worksheet': sheets[selectedSheetIdx],
							'parameters': ps,
							'parameter': ps[selectedParameterIdx],
							'ready': true
						})
					})
			})
	}

	//	Define the Save button click handler
	saveSettings(){

		//  Use the setters to save the selections
	    tableau.extensions.settings.set('measuresSheet', this.state.worksheet);
	    tableau.extensions.settings.set('parameter', this.state.parameter);

	    //  Save the state, and close the dialog box
	    tableau.extensions.settings.saveAsync().then((newSavedSettings) => {
	      tableau.extensions.ui.closeDialog();
	    });

	}

	/*	Render the component 	*/
	render() {

		let myComponent = this;

		//	Function to create menu items for each worksheet/parameter
		function createList(type) {

			//	Init an array to hold them
			let menuOptions = [];

			//	Safely fetch an array of the menu items
			let data = myComponent.state[type] ? myComponent.state[type] : [];

			//	Get the existing selection, based on the menu type
			let typeSingular = type.substring(0, type.length - 1);

			//	Iterate through them
			data.forEach( item => {
				menuOptions.push(<option type={typeSingular} key={item}>{item}</option>)
			})

			//	Return the full list
			return menuOptions;
		}

		//	Function to create a filled out dropdown
		function fullDropdowns() {
			return <div>
					<h5>Measures Worksheet:</h5> 
					<DropdownSelect onChange={e => myComponent.setState({ 'worksheet': e.target.value })} kind='line' value={myComponent.state.worksheet}>
						{createList('worksheets')}
					</DropdownSelect>
					<hr />

					<h5>Parameter:</h5>
					<DropdownSelect onChange={e => myComponent.setState({ 'parameter': e.target.value })} kind='line'  value={myComponent.state.parameter}>
						{createList('parameters')}
					</DropdownSelect>
					<hr />
				</div>
		}

		//	Function to create a placeholder dropdown
		function placeholders() {
			return <div>
					<h5>Measures Worksheet:</h5> 
					<DropdownSelect kind='line'>
					</DropdownSelect>
					<hr />

					<h5>Parameter:</h5>
					<DropdownSelect kind='line'>
					</DropdownSelect>
					<hr />
				</div>
		}

		//	Define the dropdowns to use
		let dropdowns = this.state.ready ? fullDropdowns() : placeholders();

		//	Define a button to kick off the tableau initialization
		let initButton = this.state.dev ? <Button kind="filled" key="init" style={{marginRight: 12}} onClick={ e=> myComponent.initTableau() }>Init</Button> : null;

		return  <div className="config-container">
			      <h4>Measure Selector Config</h4>
			      <p>This extension allows end users to change a parameter value by clicking on a measure.  Use this config popup, to select what worksheet to use for the list of measures and what parameter to set.</p>
			      <hr />

			      { dropdowns }
			      <Button kind="filledGreen" key="save" style={{marginRight: 12}} 
			      	onClick={ e=> myComponent.saveSettings() }>Save</Button>
			      { initButton }
			    </div>
	}
}

export default Config